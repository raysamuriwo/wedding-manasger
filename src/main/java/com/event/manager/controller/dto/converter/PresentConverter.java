package com.event.manager.controller.dto.converter;


import com.event.manager.controller.dto.PresentDTO;
import com.event.manager.model.Present;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PresentConverter {

    @Autowired
    ModelMapper modelMapper;

    public PresentDTO convertToDto(Present present) {
        PresentDTO presentDTO = modelMapper.map(present, PresentDTO.class);
        return presentDTO;
    }

    public Present convertToEntity(PresentDTO presentDTO) {
        Present present = modelMapper.map(presentDTO, Present.class);
        return present;
    }

}
