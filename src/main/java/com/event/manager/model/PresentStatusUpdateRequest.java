package com.event.manager.model;

import com.event.manager.util.Status;

public class PresentStatusUpdateRequest {
    private long presentId;
    private Status status;

    public long getPresentId() {
        return presentId;
    }

    public void setPresentId(long presentId) {
        this.presentId = presentId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PresentStatusUpdateRequest{" +
                "id=" + presentId +
                ", status=" + status +
                '}';
    }
}
