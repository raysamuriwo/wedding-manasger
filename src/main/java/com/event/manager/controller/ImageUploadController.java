package com.event.manager.controller;

import com.event.manager.model.*;
import com.event.manager.repository.*;
import com.event.manager.util.ManagerException;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/upload")
public class ImageUploadController {

    @Value("${file.storage.drive}")
    private String drive;

    @Value("${file.size.limit}")
    private long fileSizeLimit;

    @Value("${endpoint.response.success.message}")
    private String success;

    @Autowired
    private VenueRepository venueRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PresentRepository presentRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private ImageRepository imageRepository;

    @PostMapping(value = "/{userId}/{eventId}/{venueId}/{presentId}", consumes = "multipart/form-data", produces = "application/json")
    public EndpointResponse<Boolean> imageUpload(@RequestParam("files") MultipartFile [] media,
                                                 @PathVariable long userId,
                                                 @PathVariable long eventId,
                                                 @PathVariable long venueId,
                                                 @PathVariable long presentId){

        List<MultipartFile> files = new ArrayList<>(Arrays.asList(media));
        EndpointResponse<Boolean> response = new EndpointResponse<>();
        boolean isSuccess = false;

        try {
            if (userId > 0) {
                User user = userRepository.findById(userId).orElseThrow(ManagerException::new);
                List<Image> newListOfImages = saveImage(files, userId);
                for (Image image : newListOfImages) {
                    image.setUser(user);
                    image = imageRepository.save(image);
                    user.getImages().add(image);
                }
                userRepository.save(user);
            } else if (eventId > 0) {
                Event event = eventRepository.findById(eventId).orElseThrow(ManagerException::new);
                List<Image> newListOfImages = saveImage(files, userId);
                for (Image image : newListOfImages) {
                    image.setEvent(event);
                    image = imageRepository.save(image);
                    event.getImages().add(image);
                }
                eventRepository.save(event);
            } else if (venueId > 0) {
                Venue venue = venueRepository.findById(venueId).orElseThrow(ManagerException::new);
                List<Image> newListOfImages = saveImage(files, userId);
                for (Image image : newListOfImages) {
                    image.setVenue(venue);
                    image = imageRepository.save(image);
                    venue.getImages().add(image);
                }
                venueRepository.save(venue);
            } else if (presentId > 0) {
                Present present = presentRepository.findById(presentId).orElseThrow(ManagerException::new);
                List<Image> newListOfImages = saveImage(files, userId);
                for (Image image : newListOfImages) {
                    image.setPresent(present);
                    image = imageRepository.save(image);
                    present.getImages().add(image);
                }
                presentRepository.save(present);
            }
            response.setMessage(success);
            isSuccess = true;
        }catch (ManagerException ex){
            response.setMessage("You need to provide at least one valid id");
        }
        response.setSuccess(isSuccess);
        response.setData(isSuccess);
        return response;
    }

    private String renaming(long id){
        return drive +  id + "_" + UUID.randomUUID().toString();
    }

    private List<Image> saveImage(List<MultipartFile> multipartFiles, long id){
        List<Image> images = new ArrayList<>();
        if (null != multipartFiles )
        {
            for (MultipartFile multipartFile : multipartFiles) {
                try {
                    if (multipartFile.getContentType().startsWith("image/") && (multipartFile.getSize()<= fileSizeLimit)){
                        Image image = new Image();
                        String newFileName = renaming(id)+"."+ FilenameUtils.getExtension(multipartFile.getOriginalFilename());
                        Path filePath = Paths.get(newFileName);
                        Files.write(filePath, multipartFile.getBytes());
                        image.setFileLocation(newFileName);
                        image.setMime(multipartFile.getContentType());
                        image.setSize(multipartFile.getSize());
                        images.add(image);
                    }
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
        return images;
    }

}
