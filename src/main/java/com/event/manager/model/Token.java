package com.event.manager.model;

import com.event.manager.util.Status;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

@Entity
public class Token {

    private String token;
    @OneToOne
    private User user;
    @OneToOne
    private GuestUser guestUser;
    private LocalDateTime tokenCreationTime;
    private LocalDateTime expirationTime;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @PrePersist
    public void prePersist() {
        tokenCreationTime = LocalDateTime.now();
        expirationTime = tokenCreationTime.plus(30l, ChronoUnit.MINUTES);
    }

    @PreUpdate
    public void preUpdate() {
        tokenCreationTime = LocalDateTime.now();
        expirationTime = tokenCreationTime.plus(30l, ChronoUnit.MINUTES);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public GuestUser getGuestUser() {
        return guestUser;
    }

    public void setGuestUser(GuestUser guestUser) {
        this.guestUser = guestUser;
    }

    public LocalDateTime getTokenCreationTime() {
        return tokenCreationTime;
    }

    public void setTokenCreationTime(LocalDateTime tokenCreationTime) {
        this.tokenCreationTime = tokenCreationTime;
    }

    public LocalDateTime getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(LocalDateTime expirationTime) {
        this.expirationTime = expirationTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Token{" +
                "token='" + token + '\'' +
                ", user=" + user +
                ", guestUser=" + guestUser +
                ", tokenCreationTime=" + tokenCreationTime +
                ", expirationTime=" + expirationTime +
                ", id=" + id +
                '}';
    }
}
