package com.event.manager.controller.dto;

import java.util.List;

public class WishListDTO {

    private long id;
    private long eventId;
    private List<PresentDTO> presents;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<PresentDTO> getPresents() {
        return presents;
    }

    public void setPresents(List<PresentDTO> presents) {
        this.presents = presents;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    @Override
    public String toString() {
        return "WishListDTO{" +
                "id=" + id +
                ", weddingId=" + eventId +
                ", presents=" + presents +
                '}';
    }
}
