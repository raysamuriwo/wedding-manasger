package com.event.manager.controller.dto;

public class ImageDto {

    private String mime;
    private String fileLocation;
    private long size;
    private long id;


    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    @Override
    public String toString() {
        return "ImageDto{" +
                "mime='" + mime + '\'' +
                ", fileLocation='" + fileLocation + '\'' +
                ", size=" + size +
                ", id=" + id +
                '}';
    }
}
