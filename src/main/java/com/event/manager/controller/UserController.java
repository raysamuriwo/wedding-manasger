package com.event.manager.controller;

import com.event.manager.controller.dto.UserDTO;
import com.event.manager.model.EndpointResponse;
import com.event.manager.model.LoginRequest;
import com.event.manager.service.UserService;
import com.event.manager.util.ManagerException;
import com.event.manager.util.PreetyLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private PreetyLogger preetyLogger;

    @Value("${endpoint.response.success.message}")
    private String success;
    private Logger logger = LoggerFactory.getLogger(UserController.class);

    @PostMapping("/login")
    public EndpointResponse<UserDTO> login(@RequestBody LoginRequest loginRequest) {
        EndpointResponse response = new EndpointResponse();
        try {
            response.setSuccess(true);
            response.setMessage(success);
            response.setData(userService.login(loginRequest.getUsername(), loginRequest.getPassword()));
        } catch (ManagerException ex) {
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }
        catch (Exception ex){
            response.setSuccess(false);

            response.setMessage("Opps, something bad just happened");
        }
        return response;
    }

    @PostMapping(value = "/", produces = "application/json", consumes = "application/json")
    public EndpointResponse<UserDTO> createUser(@RequestBody UserDTO userDTO) {
        EndpointResponse response = new EndpointResponse();
        logger.info("Incoming user creation request {}",preetyLogger.preety(userDTO.toString()));

        try {
            response.setSuccess(true);
            response.setMessage(success);
            response.setData(userService.createUser(userDTO));
        } catch (ManagerException ex) {
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }catch (DataIntegrityViolationException ex){
            response.setSuccess(false);
            response.setMessage("Email address is already taken");
        }catch (Exception ex){
            response.setSuccess(false);
            logger.debug(ex.getMessage());
            response.setMessage("Opps, something bad just happened");
        }
        return response;
    }

}