package com.event.manager.util;

public enum Status {

    ACTIVE,
    DELETED,
    TAKEN,
    DUMMY,
    ACCEPTED,
    DECLINED

}
