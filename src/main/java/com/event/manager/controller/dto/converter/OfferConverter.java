package com.event.manager.controller.dto.converter;

import com.event.manager.controller.dto.OfferDTO;
import com.event.manager.model.Offer;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfferConverter {

    @Autowired
    ModelMapper modelMapper;

    public OfferDTO convertToDto(Offer offer) {
        OfferDTO offerDTO  = modelMapper.map(offer, OfferDTO.class);
        return offerDTO;
    }

    public Offer convertToEntity(OfferDTO offerDTO) {
        Offer offer = modelMapper.map(offerDTO, Offer.class);
        return offer;
    }
}