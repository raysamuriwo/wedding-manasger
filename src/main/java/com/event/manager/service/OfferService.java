package com.event.manager.service;

import com.event.manager.controller.dto.OfferDTO;
import com.event.manager.controller.dto.PresentDTO;

public interface OfferService {

    OfferDTO makeAnOffer(OfferDTO offerDTO) ;
    OfferDTO removePresentFromOffer(long presentId, long offerId);
    OfferDTO addPresentToOffer(PresentDTO presentDTO, long OfferID);

}
