package com.event.manager.service;

import com.event.manager.controller.dto.UserDTO;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;

public interface UserService {

    public UserDTO createUser(UserDTO userDTO) throws ParseException, NoSuchAlgorithmException, InvalidKeySpecException;
    public UserDTO login(String userName, String password) throws NoSuchAlgorithmException, InvalidKeySpecException;
    byte[] passwordHasher(String incoming) throws NoSuchAlgorithmException, InvalidKeySpecException;
}
