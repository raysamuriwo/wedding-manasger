package com.event.manager.service;

import com.event.manager.model.Token;
import com.event.manager.repository.TokenRepository;
import com.event.manager.util.ManagerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenRepository tokenRepository;

    @Override
    public Token validateUser(String tokenString) {
        Token token = tokenRepository.findTokenByToken(tokenString);
        if(token!=null || token.getExpirationTime().isAfter(LocalDateTime.now())){
           throw new ManagerException("Invalid token");
        }
        return token;
    }
}
