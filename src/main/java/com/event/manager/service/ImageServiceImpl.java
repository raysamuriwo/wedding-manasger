package com.event.manager.service;

import com.event.manager.controller.dto.converter.ImageConverter;
import com.event.manager.model.*;
import com.event.manager.repository.*;
import com.event.manager.util.ManagerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

@Service
public class ImageServiceImpl implements ImageService {

    @Autowired
    private PresentRepository presentRepository;
    @Autowired
    private ImageRepository imageRepository;
    private UserRepository userRepository;
    private VenueRepository venueRepository;
    private EventRepository eventRepository;
    private ImageConverter converter;

    @Override
    public List<Image> addOrRemoveImage(ImageOperationRequest imageOperationRequest) {

        List<Image> images;

        if(imageOperationRequest.getEventId()>0){
            Event event = eventRepository.findById(imageOperationRequest.getEventId()).orElseThrow(ManagerException::new);
            event.setImages(updateImagesListAsPerRequest(imageOperationRequest, event.getImages()));
            event = eventRepository.save(event);
            images = event.getImages();
        }else if(imageOperationRequest.getPresentId()>0){
            Present present =presentRepository.findById(imageOperationRequest.getPresentId()).orElseThrow(ManagerException::new);
            present.setImages(updateImagesListAsPerRequest(imageOperationRequest, present.getImages()));
            present = presentRepository.save(present);
            images = present.getImages();
        }else if(imageOperationRequest.getUserId()>0){
            User user =userRepository.findById(imageOperationRequest.getUserId()).orElseThrow(ManagerException::new);
            user.setImages(updateImagesListAsPerRequest(imageOperationRequest, user.getImages()));
            user = userRepository.save(user);
            images = user.getImages();
        }else if(imageOperationRequest.getVenueId()>0){
            Venue venue =venueRepository.findById(imageOperationRequest.getVenueId()).orElseThrow(ManagerException::new);
            venue.setImages(updateImagesListAsPerRequest(imageOperationRequest, venue.getImages()));
            venue = venueRepository.save(venue);
            images = venue.getImages();
        }else {
            throw new ManagerException("At least one id should be set");
        }

        return images;
    }

    private List<Image> addImage(List<Image> images, Image image){
        images.add(image);
        return images;
    }

    private List<Image> removeImage(List<Image> images, long imageid){
        Iterator<Image> imageIterator = images.iterator();
        while (imageIterator.hasNext()){
            if(imageIterator.next().getId()==imageid){
                imageRepository.deleteById(imageIterator.next().getId());
                imageIterator.remove();
            }
        }
        return images;
    }

    private List<Image> updateImagesListAsPerRequest(ImageOperationRequest imageOperationRequest, List<Image> images){
        if(imageOperationRequest.isAddImage()){
            Image image = converter.convertToEntity(imageOperationRequest.getImageDto());
            images = addImage(images, image);
        }else if(imageOperationRequest.isRemoveImage()){
            images = removeImage(images,imageOperationRequest.getImageId());
        }
        else {
            throw new ManagerException("You need to specify one action that is either to add or to remove");
        }
        return images;
    }
}
