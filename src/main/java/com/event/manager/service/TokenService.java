package com.event.manager.service;

import com.event.manager.model.Token;

public interface TokenService {

    public Token validateUser(String token);


}
