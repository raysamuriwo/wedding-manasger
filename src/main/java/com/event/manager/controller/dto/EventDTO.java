package com.event.manager.controller.dto;

import com.event.manager.model.Image;
import com.event.manager.model.User;
import com.event.manager.util.EventType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

public class EventDTO {

    private long id;
    private List<UserDTO> users;
    private VenueDTO venue;
    private WishListDTO wishList;
    private List<OfferDTO> offers;
    private String biography;
    private EventType eventType;
    @JsonIgnore
    private List<ImageDto> images;
    private List<String> contributors;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<UserDTO> getUsers() {
        return users;
    }

    public void setUsers(List<UserDTO> users) {
        this.users = users;
    }

    public VenueDTO getVenue() {
        return venue;
    }

    public void setVenue(VenueDTO venue) {
        this.venue = venue;
    }

    public WishListDTO getWishList() {
        return wishList;
    }

    public void setWishList(WishListDTO wishList) {
        this.wishList = wishList;
    }

    public List<OfferDTO> getOffers() {
        return offers;
    }

    public void setOffers(List<OfferDTO> offers) {
        this.offers = offers;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public List<String> getContributors() {
        return contributors;
    }

    public void setContributors(List<String> contributors) {
        this.contributors = contributors;
    }

    public List<ImageDto> getImages() {
        return images;
    }

    public void setImages(List<ImageDto> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "EventDTO{" +
                "id=" + id +
                ", users=" + users +
                ", venue=" + venue +
                ", wishList=" + wishList +
                ", offers=" + offers +
                ", biography='" + biography + '\'' +
                ", eventType=" + eventType +
                ", images=" + images +
                ", contributors=" + contributors +
                '}';
    }
}
