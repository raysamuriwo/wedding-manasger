package com.event.manager.controller.dto;

import java.math.BigDecimal;
import java.util.List;

public class PresentCreationDTO {
    private String presentName;
    private BigDecimal value;
    private String store;
    private long wishListId;
    private List<ImageDto> images;

    public String getPresentName() {
        return presentName;
    }

    public void setPresentName(String presentName) {
        this.presentName = presentName;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public long getWishListId() {
        return wishListId;
    }

    public void setWishListId(long wishListId) {
        this.wishListId = wishListId;
    }

    public List<ImageDto> getImages() {
        return images;
    }

    public void setImages(List<ImageDto> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "PresentCreationDTO{" +
                "presentName='" + presentName + '\'' +
                ", value=" + value +
                ", store='" + store + '\'' +
                ", wishListId=" + wishListId +
                ", images=" + images +
                '}';
    }
}
