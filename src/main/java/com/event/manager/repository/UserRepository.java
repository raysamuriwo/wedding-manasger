package com.event.manager.repository;

import com.event.manager.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface
UserRepository extends JpaRepository<User, Long> {

    User findUserByEmailAndAndPassword(String email, byte[] password);
    Optional<User> findUserByEmail(String email);

}
