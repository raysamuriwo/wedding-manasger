package com.event.manager.model;

import com.event.manager.controller.dto.VenueDTO;
import com.event.manager.util.EventType;

import java.util.List;

public class EventCreationRequest {

    private long id;
    private List<Long> userIDList;
    private VenueDTO venue;
    private String biography;
    private EventType eventType;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Long> getUserIDList() {
        return userIDList;
    }

    public void setUserIDList(List<Long> userIDList) {
        this.userIDList = userIDList;
    }

    public VenueDTO getVenue() {
        return venue;
    }

    public void setVenue(VenueDTO venue) {
        this.venue = venue;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "EventCreationRequest{" +
                "id=" + id +
                ", userIDList=" + userIDList +
                ", venue=" + venue +
                ", biography='" + biography + '\'' +
                ", eventType=" + eventType +
                '}';
    }
}
