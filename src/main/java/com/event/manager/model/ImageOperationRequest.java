package com.event.manager.model;

import com.event.manager.controller.dto.ImageDto;

public class ImageOperationRequest {

    private ImageDto imageDto;
    private long presentId;
    private long eventId;
    private long userId;
    private long venueId;
    private boolean addImage;
    private boolean removeImage;
    private long imageId;

    public ImageDto getImageDto() {
        return imageDto;
    }

    public void setImageDto(ImageDto imageDto) {
        this.imageDto = imageDto;
    }

    public long getPresentId() {
        return presentId;
    }

    public void setPresentId(long presentId) {
        this.presentId = presentId;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getVenueId() {
        return venueId;
    }

    public void setVenueId(long venueId) {
        this.venueId = venueId;
    }

    public boolean isAddImage() {
        return addImage;
    }

    public void setAddImage(boolean addImage) {
        this.addImage = addImage;
    }

    public boolean isRemoveImage() {
        return removeImage;
    }

    public void setRemoveImage(boolean removeImage) {
        this.removeImage = removeImage;
    }

    public long getImageId() {
        return imageId;
    }

    public void setImageId(long imageId) {
        this.imageId = imageId;
    }

    @Override
    public String toString() {
        return "ImageOperationRequest{" +
                "imageDto=" + imageDto +
                ", presentId=" + presentId +
                ", eventId=" + eventId +
                ", userId=" + userId +
                ", venueId=" + venueId +
                ", addImage=" + addImage +
                ", removeImage=" + removeImage +
                ", imageId=" + imageId +
                '}';
    }
}
