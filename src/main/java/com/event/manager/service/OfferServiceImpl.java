package com.event.manager.service;

import com.event.manager.controller.dto.OfferDTO;
import com.event.manager.controller.dto.PresentDTO;
import com.event.manager.controller.dto.converter.OfferConverter;
import com.event.manager.controller.dto.converter.PresentConverter;
import com.event.manager.model.Offer;
import com.event.manager.model.Event;
import com.event.manager.model.Present;
import com.event.manager.repository.EventRepository;
import com.event.manager.repository.OfferRepository;
import com.event.manager.util.ManagerException;
import org.apache.catalina.Manager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;

@Service
public class OfferServiceImpl implements OfferService {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    OfferRepository offerRepository;

    @Autowired
    PresentConverter presentConverter;

    @Autowired
    OfferConverter converter;

    @Override
    public OfferDTO makeAnOffer(OfferDTO offerDTO) {
        validateOfferDto(offerDTO);
        Event event = eventRepository.findById(offerDTO.getEventId()).orElseThrow(ManagerException::new);
        Offer offer = converter.convertToEntity(offerDTO);
        for (Present present : offer.getPresents()) {
            present.setOffer(offer);
        }
        offer.setEvent(event);
        offerRepository.save(offer);
        event.getOffers().add(offer);
        eventRepository.save(event);
        OfferDTO offerDtoResponse = converter.convertToDto(offer);
        return offerDtoResponse;
    }

    @Override
    public OfferDTO removePresentFromOffer(long presentId, long offerId) {
        Offer offer;
        if(offerId<=0) {
            throw new ManagerException("invalid offer id supplied");
        } else {
            offer = offerRepository.findById(offerId).orElseThrow(ManagerException::new);
        }
        Iterator<Present> presentIterator = offer.getPresents().iterator();
        while (presentIterator.hasNext()){
            if(presentIterator.next().getId() == presentId){
                presentIterator.remove();
            }
        }
        offer = offerRepository.save(offer);
        return converter.convertToDto(offer);
    }

    @Override
    public OfferDTO addPresentToOffer(PresentDTO presentDTO, long offerId) {
        Offer offer;
        if(offerId<=0) {
            throw new ManagerException("invalid offer id supplied");
        } else {
            offer = offerRepository.findById(offerId).orElseThrow(ManagerException::new);
        }
        Present present = presentConverter.convertToEntity(presentDTO);
        offer.getPresents().add(present);
        offer = offerRepository.save(offer);
        return converter.convertToDto(offer);
    }

    void validateOfferDto(OfferDTO offerDTO) {
        if (offerDTO.getGuestUser().getEmail().isEmpty() || offerDTO.getPresents().isEmpty() || offerDTO.getEventId() <= 0) {
            throw new ManagerException("Missing mandatory fields");
        }
    }
}
