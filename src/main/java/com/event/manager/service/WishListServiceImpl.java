package com.event.manager.service;

import com.event.manager.controller.dto.PresentCreationDTO;
import com.event.manager.controller.dto.PresentDTO;
import com.event.manager.controller.dto.WishListDTO;
import com.event.manager.controller.dto.converter.PresentConverter;
import com.event.manager.controller.dto.converter.WishListConverter;
import com.event.manager.model.Event;
import com.event.manager.model.Offer;
import com.event.manager.model.Present;
import com.event.manager.model.WishList;
import com.event.manager.repository.EventRepository;
import com.event.manager.repository.PresentRepository;
import com.event.manager.repository.WishListRepository;
import com.event.manager.util.ManagerException;
import com.event.manager.util.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class WishListServiceImpl implements WishListService {

    @Autowired
    WishListConverter converter;

    @Autowired
    EventRepository weddingRepository;

    @Autowired
    PresentRepository presentRepository;

    @Autowired
    WishListRepository repository;

    @Autowired
    PresentConverter presentConverter;

    @Override
    @Transactional
    public WishListDTO createWishList(WishListDTO wishListDTO) {
        if (wishListDTO.getEventId() <= 0) {
            throw new ManagerException("Invalid event id supplied");
        }

        Event event = weddingRepository.findById(wishListDTO.getEventId()).orElseThrow(ManagerException::new);
        WishList wishList = converter.convertToEntity(wishListDTO);
        for(Present present : wishList.getPresents()){
            present.setWishList(wishList);
        }
        wishList = repository.save(wishList);
        event.setWishList(wishList);
        event = weddingRepository.save(event);
        return converter.convertToDto(event.getWishList());

    }

    @Override
    public WishListDTO updateWishList(WishListDTO wishListDTO) {
        if (wishListDTO.getEventId() <= 0) {
            throw new ManagerException("Invalid event id supplied");
        }
        WishList wishListRequest = converter.convertToEntity(wishListDTO);
        WishList wishListFromDb = repository.findById(wishListDTO.getId()).orElseThrow(ManagerException::new);
        if(!wishListRequest.getPresents().isEmpty()){
            for(Present present: wishListRequest.getPresents()) {
                if (present.getId() <= 0) {
                    present.setWishList(wishListFromDb);
                    wishListFromDb.getPresents().add(present);
                }
            }
        }
        wishListFromDb = repository.save(wishListFromDb);
        wishListDTO = converter.convertToDto(wishListFromDb);
        return wishListDTO;

    }

    @Override
    public WishListDTO addPresent(PresentCreationDTO creationDTO) {
        if (creationDTO.getWishListId() <= 0) {
            throw new ManagerException("Invalid event id supplied");
        }
        WishList wishList = repository.findById(creationDTO.getWishListId()).orElseThrow(ManagerException::new);
        Present present = new Present();
        present.setWishList(wishList);
        present.setPresentName(creationDTO.getPresentName());
        present.setStore(creationDTO.getStore());
        present.setValue(creationDTO.getValue());
        wishList.getPresents().add(present);
        wishList = repository.save(wishList);
        return converter.convertToDto(wishList);
    }

    @Override
    public PresentDTO changePresentStatus(long presentId, Status status) {
        if (presentId <= 0) {
            throw new ManagerException("Invalid event id supplied");
        }
        Present present = presentRepository.findById(presentId).orElseThrow(ManagerException::new);
        present.setStatus(status);
        present = presentRepository.save(present);
        return presentConverter.convertToDto(present);
    }

    @Override
    public WishListDTO removePresentFromWishList(long presentId, long wishListId) {
        WishList wishList;
        if(wishListId<=0) {
            throw new ManagerException("invalid wishLi id supplied");
        } else {
            wishList = repository.findById(wishListId).orElseThrow(ManagerException::new);
        }
        Iterator<Present> presentIterator = wishList.getPresents().iterator();
        while (presentIterator.hasNext()){
            if(presentIterator.next().getId() == presentId){
                presentIterator.remove();
            }
        }
        wishList = repository.save(wishList);
        return converter.convertToDto(wishList);
    }

    @Override
    public WishListDTO addPresentToWishList(PresentDTO presentDTO, long wishListId) {
        WishList wishList;
        if(wishListId<=0) {
            throw new ManagerException("invalid wishlist id supplied");
        } else {
            wishList = repository.findById(wishListId).orElseThrow(ManagerException::new);
        }
        Present present = presentConverter.convertToEntity(presentDTO);
        wishList.getPresents().add(present);
        wishList = repository.save(wishList);
        return converter.convertToDto(wishList);
    }
}
