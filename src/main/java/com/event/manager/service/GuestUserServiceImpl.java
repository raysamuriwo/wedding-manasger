package com.event.manager.service;

import com.event.manager.controller.dto.GuestUserDTO;
import com.event.manager.controller.dto.UserDTO;
import com.event.manager.controller.dto.converter.GuestUserConverter;
import com.event.manager.controller.dto.converter.UserConverter;
import com.event.manager.model.GuestUser;
import com.event.manager.model.LoginRequest;
import com.event.manager.model.Token;
import com.event.manager.model.User;
import com.event.manager.repository.GuestUserRepository;
import com.event.manager.repository.TokenRepository;
import com.event.manager.repository.UserRepository;
import com.event.manager.util.ManagerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.text.ParseException;
import java.util.UUID;

@Service
public class GuestUserServiceImpl implements GuestUserService {

    @Value("${token.time.to.live}")
    private long timeToLive;

    @Value("${user.password.salt}")
    private String salt;

    @Autowired
    private GuestUserRepository repository;

    @Autowired
    private GuestUserConverter converter;

    @Autowired
    private TokenRepository tokenRepository;

    @Override
    public GuestUserDTO createUser(GuestUserDTO guestUserDTO) throws ParseException, InvalidKeySpecException, NoSuchAlgorithmException {
        validate(guestUserDTO);
        GuestUser guestUser = converter.convertToEntity(guestUserDTO);
        guestUser.setPassword(passwordHasher(guestUserDTO.getPassword()));
        guestUser = repository.save(guestUser);
        guestUserDTO = converter.convertToDto(guestUser);
        return guestUserDTO;
    }

    @Override
    public GuestUserDTO login(String userName, String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        if (userName.isEmpty() || password.isEmpty()) {
            throw new ManagerException("Username or Password can not be null");
        }
        GuestUser user = repository.findGuestUserByEmailAndPassword(userName, passwordHasher(password));
        if (user == null) {
            throw new ManagerException("Invalid username or password");
        }
        Token token = tokenRepository.findTokenByUser_Id(user.getId());
        if (token == null) {
            token.setGuestUser(user);
            token.setToken(UUID.randomUUID().toString());
            token.setTimeToLive(timeToLive);
            tokenRepository.save(token);
        }
        GuestUserDTO dto = converter.convertToDto(user);
        dto.setAuthenticationString(token.getToken());
        dto.setPassword(null);
        return dto;
    }

    private void validate(GuestUserDTO userDTO) {
        if (userDTO.getEmail().isEmpty()) {
            throw new ManagerException("Email can not be empt");
        }
        if (userDTO.getName().isEmpty()) {
            throw new ManagerException("Firstname can not be empty");
        }
        if (userDTO.getPassword().isEmpty()) {
            throw new ManagerException("Mobile number can not be empty");
        }
        if (userDTO.getEmail().isEmpty()) {
            throw new ManagerException("Surname can not be empty");
        }
    }

    private byte[] passwordHasher(String incoming) throws NoSuchAlgorithmException, InvalidKeySpecException {

        KeySpec spec = new PBEKeySpec(incoming.toCharArray(), salt.getBytes(), 65536, 128);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        return factory.generateSecret(spec).getEncoded();
    }
}
