package com.event.manager.controller;


import com.event.manager.controller.dto.OfferDTO;
import com.event.manager.controller.dto.PresentCreationDTO;
import com.event.manager.controller.dto.PresentDTO;
import com.event.manager.controller.dto.WishListDTO;
import com.event.manager.model.EndpointResponse;
import com.event.manager.model.PresentStatusUpdateRequest;
import com.event.manager.service.WishListService;
import com.event.manager.util.ManagerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("wishList")
public class WishListController {


    @Autowired
    private WishListService wishListService;

    @Value("${endpoint.response.success.message}")
    private String success;


    @PostMapping(value = "/", produces = "application/json", consumes = "application/json")
    public EndpointResponse<WishListDTO> createWishList(@RequestBody WishListDTO wishListDTO){
        EndpointResponse response = new EndpointResponse();
        try {
            response.setSuccess(true);
            response.setMessage(success);
            WishListDTO dto = wishListService.createWishList(wishListDTO);
            response.setData(dto);
        } catch (ManagerException ex) {
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    @PutMapping(value = "/present", produces = "application/json", consumes = "application/json")
    public EndpointResponse<WishListDTO> addPresentToWishList(@RequestBody PresentCreationDTO request){

        EndpointResponse response = new EndpointResponse();
        try {
            response.setSuccess(true);
            response.setMessage(success);
            response.setData(wishListService.addPresent(request));
        } catch (ManagerException ex) {
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    @PutMapping(value = "/updatePresent" , produces = "application/json", consumes = "application/json")
    public EndpointResponse<PresentDTO> updatePresentStatus(@RequestBody PresentStatusUpdateRequest request){
        EndpointResponse response = new EndpointResponse();
        try {
            response.setSuccess(true);
            response.setMessage(success);
            response.setData(wishListService.changePresentStatus(request.getPresentId(), request.getStatus()));
        } catch (ManagerException ex) {
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    @GetMapping(value = "/{wishListId}/{presentId}", produces = "application/json")
    public EndpointResponse<OfferDTO> removePresent(@RequestParam("wishListId")long wishListId,
                                                    @RequestParam("") long presentId){
        EndpointResponse response = new EndpointResponse();
        try {
            response.setSuccess(true);
            response.setMessage(success);
            response.setData(wishListService.removePresentFromWishList(presentId,wishListId));
        }
        catch (ManagerException ex){
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    @PutMapping(value = "/{wishListId}", produces = "application/json")
    public EndpointResponse<OfferDTO> addPresent(@RequestParam long wishListId,
                                                    @RequestBody PresentDTO presentDTO){
        EndpointResponse response = new EndpointResponse();
        try {
            response.setSuccess(true);
            response.setMessage(success);
            response.setData(wishListService.addPresentToWishList(presentDTO,wishListId));
        }
        catch (ManagerException ex){
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }



}
