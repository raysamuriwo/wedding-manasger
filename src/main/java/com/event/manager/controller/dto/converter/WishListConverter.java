package com.event.manager.controller.dto.converter;

import com.event.manager.controller.dto.WishListDTO;
import com.event.manager.model.WishList;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WishListConverter {

    @Autowired
    ModelMapper modelMapper;

    public WishListDTO convertToDto(WishList wishList) {
        WishListDTO wishListDTO = modelMapper.map(wishList, WishListDTO.class);
        return wishListDTO;
    }

    public WishList convertToEntity(WishListDTO wishListDTO) {
        WishList wishList = modelMapper.map(wishListDTO, WishList.class);
        return wishList;
    }

}
