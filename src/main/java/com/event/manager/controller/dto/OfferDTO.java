package com.event.manager.controller.dto;

import com.event.manager.model.GuestUser;
import com.event.manager.model.Present;

import java.math.BigDecimal;
import java.util.List;

public class OfferDTO {

    private long id;
    private GuestUserDTO guestUser;
    private long eventId;
    private List<PresentDTO> presents;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public GuestUserDTO getGuestUser() {
        return guestUser;
    }

    public void setGuestUser(GuestUserDTO guestUser) {
        this.guestUser = guestUser;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public List<PresentDTO> getPresents() {
        return presents;
    }

    public void setPresents(List<PresentDTO> presents) {
        this.presents = presents;
    }
}
