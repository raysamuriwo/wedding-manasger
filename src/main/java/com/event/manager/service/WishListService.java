package com.event.manager.service;

import com.event.manager.controller.dto.PresentCreationDTO;
import com.event.manager.controller.dto.PresentDTO;
import com.event.manager.controller.dto.WishListDTO;
import com.event.manager.util.Status;

public interface  WishListService {

    public WishListDTO createWishList(WishListDTO wishListDTO);
    public WishListDTO updateWishList(WishListDTO wishListDTO);
    public WishListDTO addPresent(PresentCreationDTO creationDTO);
    public PresentDTO changePresentStatus(long presentId, Status status);
    public WishListDTO removePresentFromWishList(long presentId, long wishListId);
    public WishListDTO addPresentToWishList(PresentDTO present, long wishListId);
}
