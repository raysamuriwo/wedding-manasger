package com.event.manager.controller;

import com.event.manager.controller.dto.EventDTO;
import com.event.manager.model.EndpointResponse;
import com.event.manager.model.EventCreationRequest;
import com.event.manager.model.Page;
import com.event.manager.service.EventService;
import com.event.manager.util.ManagerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("event")
public class EventController {

    @Value("${endpoint.response.success.message}")
    private String success;
    @Value("${endpoint.response.error.message}")
    private String errorMessage;
    private Logger logger = LoggerFactory.getLogger(EventController.class);

    @Autowired
    EventService eventService;

    @PostMapping(value = "/", produces = "application/json")
    public EndpointResponse<EventDTO> createEvent(@RequestBody EventCreationRequest request){
        EndpointResponse response = new EndpointResponse();
        try{
            EventDTO weddingDTO = eventService.createEvent(request);
            response.setData(weddingDTO);
            response.setMessage(success);
            response.setSuccess(true);
        }catch (ManagerException ex){
            response.setMessage(ex.getMessage());
            response.setSuccess(false);
        }catch (ParseException ex){
            logger.debug(ex.getMessage());
            response.setMessage(errorMessage);
            response.setSuccess(false);
        }
        return response;
    }

    @PutMapping(value = "/", produces = "application/json")
    public EndpointResponse<EventDTO> updateEvent(@RequestBody EventDTO weddingDTO){
        EndpointResponse response = new EndpointResponse();
        try{
            response.setData(eventService.updateEvent(weddingDTO));
            response.setMessage(success);
            response.setSuccess(true);
        }catch (ManagerException ex){
            response.setMessage(ex.getMessage());
            response.setSuccess(false);
        }catch (ParseException ex){
            logger.debug(ex.getMessage());
            response.setMessage(errorMessage);
            response.setSuccess(false);
        }
        return response;
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public EndpointResponse<EventDTO> getEvent(@PathVariable long id){
        EndpointResponse response = new EndpointResponse();
        try{
            response.setData(eventService.getEvent(id));
            response.setMessage(success);
            response.setSuccess(true);
        }catch (ManagerException ex) {
            response.setMessage(ex.getMessage());
            response.setSuccess(false);
        }
        return response;
    }

    @PostMapping(value = "/pages", produces = "application/json", consumes = "application/json")
    public EndpointResponse<EventDTO> getAllEvents(@RequestBody Page page){
        EndpointResponse response = new EndpointResponse();
        try{
            response.setData(eventService.getEvents(new PageRequest(page.getPageNumber(), page.getPageSize())));
            response.setMessage(success);
            response.setSuccess(true);
        }catch (ManagerException ex) {
            response.setMessage(ex.getMessage());
            response.setSuccess(false);
        }
        return response;
    }

    @GetMapping(value = "/user/{id}", produces = "application/json")
    public EndpointResponse<List<EventDTO>> getEventsByUser(@PathVariable long id){
        EndpointResponse response = new EndpointResponse();
        try{
            response.setData(eventService.getEventsByUser(id));
            response.setMessage(success);
            response.setSuccess(true);
        }catch (ManagerException ex) {
            response.setMessage(ex.getMessage());
            response.setSuccess(false);
        }
        return response;
    }

    @PostMapping(value = "/{contributor}/{eventId}", consumes = "application/json", produces = "application/json")
    public EndpointResponse<List<String>> removeContributor(@PathVariable String contributor,
                                                            @PathVariable long eventId ){
        EndpointResponse<List<String>> response = new EndpointResponse();
        try {
            response.setSuccess(true);
            response.setMessage(success);
            response.setData(eventService.removeContributor(contributor, eventId));
        }
        catch (ManagerException ex){
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    @PutMapping(value = "/{contributor}/{eventId}", consumes = "application/json", produces = "application/json")
    public EndpointResponse<List<String>> addContributor(@PathVariable String contributor,
                                                            @PathVariable long eventId ){
        EndpointResponse<List<String>> response = new EndpointResponse();
        try {
            response.setMessage(success);
            response.setSuccess(true);
            response.setData(eventService.removeContributor(contributor, eventId));
        }
        catch (ManagerException ex){
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }
}
