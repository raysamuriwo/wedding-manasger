package com.event.manager.controller.dto;

import com.event.manager.util.PresentType;
import com.event.manager.util.Status;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;
import java.util.List;

public class PresentDTO {

    private long id;
    private String presentName;
    private BigDecimal value;
    private String store;
    private Status status;
    private PresentType presentType;
    private List<ImageDto> images;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPresentName() {
        return presentName;
    }

    public void setPresentName(String presentName) {
        this.presentName = presentName;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public PresentType getPresentType() {
        return presentType;
    }

    public void setPresentType(PresentType presentType) {
        this.presentType = presentType;
    }

    public List<ImageDto> getImages() {
        return images;
    }

    public void setImages(List<ImageDto> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "PresentDTO{" +
                "id=" + id +
                ", presentName='" + presentName + '\'' +
                ", value=" + value +
                ", store='" + store + '\'' +
                ", status=" + status +
                ", presentType=" + presentType +
                ", images=" + images +
                '}';
    }
}
