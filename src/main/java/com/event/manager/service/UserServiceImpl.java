package com.event.manager.service;

import com.event.manager.controller.dto.UserDTO;
import com.event.manager.controller.dto.converter.UserConverter;
import com.event.manager.model.Token;
import com.event.manager.model.User;
import com.event.manager.repository.TokenRepository;
import com.event.manager.repository.UserRepository;
import com.event.manager.util.ManagerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.text.ParseException;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository repository;

    @Autowired
    private UserConverter converter;

    @Autowired
    private TokenRepository tokenRepository;

    @Value("${user.password.salt}")
    private String salt;

    @Value("${token.time.to.live}")
    private long timeToLive;

    @Override
    public UserDTO createUser(UserDTO userDTO) throws ParseException, InvalidKeySpecException, NoSuchAlgorithmException {
        validate(userDTO);
        User user = converter.convertToEntity(userDTO);
        user.setPassword(passwordHasher(userDTO.getPassword()));
        userDTO.setAuthenticationString(null);
        user = repository.save(user);
        userDTO = converter.convertToDto(user);
        return userDTO;
    }

    @Override
    public UserDTO login(String userName, String password)  throws NoSuchAlgorithmException, InvalidKeySpecException{
        if(userName.isEmpty() || password.isEmpty()){
            throw new ManagerException("Username or Password can not be null");
        }
        User user = repository.findUserByEmailAndAndPassword(userName, passwordHasher(password));
        if( user == null){
            throw new ManagerException("Invalid username or password");
        }
        Token token = tokenRepository.findTokenByUser_Id(user.getId());
        if(token == null){
            token.setUser(user);
            token.setToken(UUID.randomUUID().toString());
            token.setTimeToLive(timeToLive);
            tokenRepository.save(token);
        }
        UserDTO dto = converter.convertToDto(user);
        dto.setAuthenticationString(token.getToken());
        dto.setPassword(null);
        return dto ;
    }

    @Override
    public byte[] passwordHasher(String incoming) throws NoSuchAlgorithmException, InvalidKeySpecException {

        KeySpec spec = new PBEKeySpec(incoming.toCharArray(), salt.getBytes(), 65536, 128);
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        return factory.generateSecret(spec).getEncoded();
    }

    private void validate(UserDTO userDTO){
        if(userDTO.getEmail().isEmpty()){
            throw new ManagerException("Email can not be empt");
        }
        if(userDTO.getFirstname().isEmpty()){
            throw new ManagerException("Firstname can not be empty");
        }
        if(userDTO.getMobileNumber().isEmpty()){
            throw new ManagerException("Mobile number can not be empty");
        }
        if(userDTO.getSurname().isEmpty()){
            throw new ManagerException("Surname can not be empty");
        }
        if(userDTO.getAuthenticationString().isEmpty()){
            throw new ManagerException("Password can not be empty");
        }
    }

}
