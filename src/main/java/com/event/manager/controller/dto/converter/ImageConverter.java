package com.event.manager.controller.dto.converter;

import com.event.manager.controller.dto.GuestUserDTO;
import com.event.manager.controller.dto.ImageDto;
import com.event.manager.model.GuestUser;
import com.event.manager.model.Image;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImageConverter {

    @Autowired
    ModelMapper modelMapper;

    public ImageDto convertToDto(Image image) {
        ImageDto imageDto = modelMapper.map(image, ImageDto.class);
        return imageDto;
    }

    public Image convertToEntity(ImageDto imageDto) {
        Image image = modelMapper.map(imageDto, Image.class);
        return image;
    }


}
