package com.event.manager.repository;

import com.event.manager.model.Present;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PresentRepository extends JpaRepository<Present, Long> {
}
