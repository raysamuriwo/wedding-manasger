package com.event.manager.service;

import com.event.manager.controller.dto.GuestUserDTO;
import com.event.manager.model.GuestUser;
import com.event.manager.model.LoginRequest;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;

public interface GuestUserService {

    public GuestUserDTO createUser(GuestUserDTO guestUserDTO) throws ParseException, InvalidKeySpecException, NoSuchAlgorithmException;
    public GuestUserDTO login(String userName, String password) throws NoSuchAlgorithmException, InvalidKeySpecException;
}
