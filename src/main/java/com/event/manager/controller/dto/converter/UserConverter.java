package com.event.manager.controller.dto.converter;

import com.event.manager.controller.dto.UserDTO;
import com.event.manager.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;

@Service
public class UserConverter {

    @Autowired
    ModelMapper modelMapper;

    public UserDTO convertToDto(User user) {
        UserDTO userDTO  = modelMapper.map(user, UserDTO.class);
        userDTO.setPassword(null);
        return userDTO;
    }

    public User convertToEntity(UserDTO userDTO) throws ParseException {
        User user = modelMapper.map(userDTO, User.class);
        return user;
    }
}

