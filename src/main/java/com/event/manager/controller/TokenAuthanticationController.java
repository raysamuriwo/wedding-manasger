package com.event.manager.controller;

import com.event.manager.controller.dto.OfferDTO;
import com.event.manager.model.EndpointResponse;
import com.event.manager.model.Token;
import com.event.manager.service.OfferService;
import com.event.manager.service.TokenService;
import com.event.manager.util.ManagerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("token")
public class TokenAuthanticationController {

    @Autowired
    private TokenService tokenService;

    @Value("${endpoint.response.success.message}")
    private String success;

    @GetMapping(value = "/{token}", consumes = "application/json", produces = "application/json")
    public EndpointResponse<OfferDTO> placeAnOffer(@PathVariable String tokenString){
        EndpointResponse response = new EndpointResponse();
        try {
            response.setSuccess(true);
            response.setMessage(success);
            Token token = tokenService.validateUser(tokenString);
            response.setData((token.getGuestUser() != null) ? token.getGuestUser() : token.getUser());
        }
        catch (ManagerException ex){
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }
}
