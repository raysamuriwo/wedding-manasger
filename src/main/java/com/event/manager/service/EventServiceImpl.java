package com.event.manager.service;

import com.event.manager.controller.dto.EventDTO;
import com.event.manager.controller.dto.VenueDTO;
import com.event.manager.controller.dto.converter.*;
import com.event.manager.model.Event;
import com.event.manager.model.EventCreationRequest;
import com.event.manager.model.User;
import com.event.manager.model.Venue;
import com.event.manager.repository.EventRepository;
import com.event.manager.repository.UserRepository;
import com.event.manager.repository.VenueRepository;
import com.event.manager.util.ManagerException;
import com.event.manager.util.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventConverter converter;

    @Autowired
    private EventRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private VenueConverter venueConverter;

    @Autowired
    private VenueRepository venueRepository;

    @Override
    public List<EventDTO> getEventsByUser(long userId) {
        if(userId<=0){
            throw new ManagerException("Invalid user id supplied");
        }
        User user = userRepository.findById(userId).orElseThrow(ManagerException::new);
        List<EventDTO> eventDTOList = new ArrayList<>();
        for(Event event: user.getEvents()) {
            EventDTO dto = converter.convertToDto(event);
            eventDTOList.add(dto);
        }
        return eventDTOList;
    }
    @Override
    public EventDTO createEvent(EventCreationRequest request) throws ParseException {
        Venue venue;
        if (request.getVenue() !=null) {
            venue = venueConverter.convertToEntity(request.getVenue());
        }else {
            throw new ManagerException("Venue must be supplied");
        }
        List<User> users = new ArrayList<>();
        if(!request.getUserIDList().isEmpty()){
            for(long id: request.getUserIDList()){
                User user = userRepository.findById(id).orElseThrow(ManagerException::new);
                users.add(user);
            }
        }else {
            throw new ManagerException("Users must be supplied");
        }

        Event event = new Event();
        event.setVenue(venue);
        event.setBiography(request.getBiography());
        event.setVenue(venue);
        event.setUsers(users);
        event = repository.save(event);
        return converter.convertToDto(event);
    }

    @Override
    public EventDTO getEvent(long id) {
        if(id<=0){
            throw new ManagerException("Invalid ID supplied");
        }
        Event event =  repository.findById(id).orElseThrow(ManagerException::new);
        return converter.convertToDto(event);
    }

    @Override
    public EventDTO updateEvent(EventDTO eventDTO) throws ParseException {

        if(eventDTO.getId()<=0){
            throw new ManagerException("Please provide a valid identifier");
        }

        Event event = repository.findById(eventDTO.getId()).orElseThrow(ManagerException::new);
        Event incomingEvent = converter.convertToEntity(eventDTO);

        if(eventDTO.getVenue().getId()>0){
            Venue venue = venueRepository.getOne(eventDTO.getVenue().getId());
            event.setVenue(venueConverter.convertToEntity(eventDTO.getVenue()));
        }
        if (!incomingEvent.getBiography().isEmpty()){
            event.setBiography(incomingEvent.getBiography());
        }
        if(incomingEvent.getEventType()!=null){
            event.setEventType(incomingEvent.getEventType());
        }

        return converter.convertToDto(repository.save(event));
    }

    @Override
    public Page<EventDTO> getEvents(PageRequest pageable) {
        Page<Event> eventPage =  repository.findAllByStatus(Status.ACTIVE,pageable);
        return eventPage.map(converter :: convertToDto);
    }

    @Override
    public List<String> addContributor(String contributor, long eventId) {
        Event event = repository.findById(eventId).orElseThrow(ManagerException::new);
        event.getContributors().add(contributor);
        return event.getContributors();
    }

    @Override
    public List<String> removeContributor(String contributor, long eventId) {
        Event event = repository.findById(eventId).orElseThrow(ManagerException::new);
        event.getContributors().remove(contributor);
        return event.getContributors();
    }

    @Override
    public VenueDTO updateVenue(VenueDTO venueDTO) {
        Venue venue = venueRepository.getOne(venueDTO.getId());
        venue.setLatitude((venueDTO.getLatitude()!=null) ? venueDTO.getLatitude() : venue.getLatitude());
        venue.setLongitude((venueDTO.getLongitude()!=null) ? venueDTO.getLongitude() : venue.getLongitude());
        venue.setVenueName(venueDTO.getName());
        return null;
    }
}
