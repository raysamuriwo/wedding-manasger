package com.event.manager.util;

public class ManagerException extends RuntimeException{

    public ManagerException() {
        super();
    }

    public ManagerException(String s) {
        super(s);
    }
}
