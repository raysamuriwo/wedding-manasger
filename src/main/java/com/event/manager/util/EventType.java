package com.event.manager.util;

public enum EventType {

    WEDDING,
    KITCHEN_PARTY,
    BIRTHDAY,
    GET_TOGETHER,
    GRADUATION,
    BABY_WELCOME

}
