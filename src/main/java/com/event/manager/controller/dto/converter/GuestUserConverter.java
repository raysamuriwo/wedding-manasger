package com.event.manager.controller.dto.converter;

import com.event.manager.controller.dto.GuestUserDTO;
import com.event.manager.controller.dto.OfferDTO;
import com.event.manager.model.GuestUser;
import com.event.manager.model.Offer;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GuestUserConverter {

    @Autowired
    ModelMapper modelMapper;

    public GuestUserDTO convertToDto(GuestUser user) {
        GuestUserDTO userDTO = modelMapper.map(user, GuestUserDTO.class);
        userDTO = null;
        return userDTO;
    }

    public GuestUser convertToEntity(GuestUserDTO userDTO) {
        GuestUser user = modelMapper.map(userDTO, GuestUser.class);
        return user;
    }

}