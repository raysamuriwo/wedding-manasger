package com.event.manager.repository;

import com.event.manager.model.Token;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends CrudRepository<Token, Long> {

    Token findTokenByUser_Id(long id);
    Token findTokenByToken(String token);

}
