package com.event.manager.service;

import com.event.manager.controller.dto.EventDTO;
import com.event.manager.controller.dto.VenueDTO;
import com.event.manager.model.EventCreationRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import java.text.ParseException;
import java.util.List;

public interface EventService {

    public List<EventDTO> getEventsByUser(long userId);
    public EventDTO getEvent(long weddingId);
    public EventDTO createEvent(EventCreationRequest request) throws ParseException;
    public EventDTO updateEvent(EventDTO weddingDTO)throws ParseException;
    public Page<EventDTO> getEvents(PageRequest pageable);
    public List<String> addContributor(String contributor, long eventId);
    public List<String> removeContributor(String contributor, long eventId);
    public VenueDTO updateVenue(VenueDTO venueDTO);

}
