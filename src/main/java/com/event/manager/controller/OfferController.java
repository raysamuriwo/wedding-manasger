package com.event.manager.controller;

import com.event.manager.controller.dto.OfferDTO;
import com.event.manager.controller.dto.PresentDTO;
import com.event.manager.model.EndpointResponse;
import com.event.manager.model.Image;
import com.event.manager.service.OfferService;
import com.event.manager.util.ManagerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("offer")
public class OfferController {

    @Autowired
    OfferService offerService;

    @Value("${endpoint.response.success.message}")
    private String success;

    @PostMapping(value = "/", consumes = "application/json", produces = "application/json")
    public EndpointResponse<OfferDTO> placeAnOffer(@RequestBody OfferDTO offerDTO){
        EndpointResponse response = new EndpointResponse();
        try {
            response.setSuccess(true);
            response.setMessage(success);
            response.setData(offerService.makeAnOffer(offerDTO));
        }
        catch (ManagerException ex){
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    @GetMapping(value = "/{offerId}/{presentId}", produces = "application/json")
    public EndpointResponse<OfferDTO> removePresent(@RequestParam("offerId")long offerId,
                                                    @RequestParam("") long presentId){
        EndpointResponse response = new EndpointResponse();
        try {
            response.setSuccess(true);
            response.setMessage(success);
            response.setData(offerService.removePresentFromOffer(presentId,offerId));
        }
        catch (ManagerException ex){
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

    @PutMapping(value = "/{offerId}", produces = "application/json")
    public EndpointResponse<OfferDTO> addPresent(@RequestParam long offerId,
                                                    @RequestBody PresentDTO presentDTO){
        EndpointResponse response = new EndpointResponse();
        try {
            response.setSuccess(true);
            response.setMessage(success);
            response.setData(offerService.addPresentToOffer(presentDTO,offerId));
        }
        catch (ManagerException ex){
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }
        return response;
    }

}
