package com.event.manager.service;

import com.event.manager.controller.dto.PresentDTO;

public interface PresentService {

    public PresentDTO updatePresent(PresentDTO presentDTO);
}
