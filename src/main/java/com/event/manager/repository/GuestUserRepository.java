package com.event.manager.repository;

import com.event.manager.controller.dto.GuestUserDTO;
import com.event.manager.model.GuestUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GuestUserRepository extends JpaRepository<GuestUser, Long> {

    GuestUser findGuestUserByEmailAndPassword(String email, byte[] password);

}
