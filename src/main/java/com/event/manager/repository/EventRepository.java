package com.event.manager.repository;

import com.event.manager.model.Event;
import com.event.manager.model.User;
import com.event.manager.util.Status;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;
import java.util.Optional;

public interface EventRepository extends PagingAndSortingRepository<Event, Long> {

    Page<Event> findAllByStatus(Status status, Pageable pageable);

}
