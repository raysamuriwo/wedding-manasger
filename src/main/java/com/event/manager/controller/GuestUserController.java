package com.event.manager.controller;

import com.event.manager.controller.dto.GuestUserDTO;
import com.event.manager.controller.dto.UserDTO;
import com.event.manager.model.EndpointResponse;
import com.event.manager.model.GuestUser;
import com.event.manager.model.LoginRequest;
import com.event.manager.service.GuestUserService;
import com.event.manager.util.ManagerException;
import com.event.manager.util.PreetyLogger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("guestUsers")
public class GuestUserController {

    @Autowired
    private PreetyLogger preetyLogger;

    @Autowired
    private GuestUserService service;

    @Value("${endpoint.response.success.message}")
    private String success;
    private Logger logger = LoggerFactory.getLogger(UserController.class);


    @PostMapping("/login")
    public EndpointResponse<GuestUserDTO> login(@RequestBody LoginRequest loginRequest) {
        EndpointResponse<GuestUserDTO> response = new EndpointResponse();

    try {
        response.setSuccess(true);
        response.setMessage(success);
        response.setData(service.login(loginRequest));
    } catch (
    ManagerException ex) {
        response.setSuccess(false);
        response.setMessage(ex.getMessage());
    }
        catch (Exception ex){
        response.setSuccess(false);

        response.setMessage("Opps, something bad just happened");
    }
        return response;
}

    @PostMapping(value = "/", produces = "application/json", consumes = "application/json")
    public EndpointResponse<UserDTO> createUser(@RequestBody GuestUserDTO userDTO) {
        EndpointResponse response = new EndpointResponse();
        logger.info("Incoming user creation request {}",preetyLogger.preety(userDTO.toString()));

        try {
            response.setSuccess(true);
            response.setMessage(success);
            response.setData(service.newGuest(userDTO));
        } catch (ManagerException ex) {
            response.setSuccess(false);
            response.setMessage(ex.getMessage());
        }catch (DataIntegrityViolationException ex){
            response.setSuccess(false);
            response.setMessage("Email address is already taken");
        }catch (Exception ex){
            response.setSuccess(false);
            logger.debug(ex.getMessage());
            response.setMessage("Opps, something bad just happened");
        }
        return response;
    }

}
