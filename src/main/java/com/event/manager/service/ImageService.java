package com.event.manager.service;

import com.event.manager.controller.dto.PresentDTO;
import com.event.manager.model.Image;
import com.event.manager.model.ImageOperationRequest;

import java.util.List;

public interface ImageService {

    public List<Image> addOrRemoveImage(ImageOperationRequest imageOperationRequest);

}
