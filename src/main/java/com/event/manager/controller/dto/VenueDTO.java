package com.event.manager.controller.dto;

import com.event.manager.model.Image;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VenueDTO {

    private long id;
    private String venueName;
    private String latitude;
    private String longitude;
    private List<Image> images;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return venueName;
    }

    public void setName(String name) {
        this.venueName = name;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    @Override
    public String toString() {
        return "VenueDTO{" +
                "id=" + id +
                ", venueName='" + venueName + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", images=" + images +
                '}';
    }
}

