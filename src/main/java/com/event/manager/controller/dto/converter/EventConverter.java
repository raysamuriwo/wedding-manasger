package com.event.manager.controller.dto.converter;

import com.event.manager.controller.dto.EventDTO;
import com.event.manager.model.Event;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;

@Service
public class EventConverter {

    @Autowired
    ModelMapper modelMapper;

    public EventDTO convertToDto(Event wedding) {
        EventDTO weddingDTO  = modelMapper.map(wedding, EventDTO.class);
        return weddingDTO;
    }

    public Event convertToEntity(EventDTO weddingDTO) throws ParseException {
        Event wedding = modelMapper.map(weddingDTO, Event.class);
        return wedding;
    }
}
