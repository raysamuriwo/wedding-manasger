package com.event.manager.controller.dto.converter;

import com.event.manager.controller.dto.VenueDTO;
import com.event.manager.model.Venue;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.text.ParseException;

@Service
public class VenueConverter {

    @Autowired
    ModelMapper modelMapper;

    public VenueDTO convertToDto(Venue venue) {
        VenueDTO venueDTO = modelMapper.map(venue, VenueDTO.class);
        return venueDTO;
    }

    public Venue convertToEntity(VenueDTO venueDTO) throws ParseException {
        Venue venue = modelMapper.map(venueDTO, Venue.class);
        return venue;
    }

}
