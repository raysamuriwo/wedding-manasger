package com.event.manager.service;

import com.event.manager.controller.dto.PresentDTO;
import com.event.manager.controller.dto.converter.PresentConverter;
import com.event.manager.model.Present;
import com.event.manager.repository.PresentRepository;
import com.event.manager.util.ManagerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PresentServiceImpl implements PresentService {

    @Autowired
    PresentRepository presentRepository;

    @Autowired
    PresentConverter converter;

    @Override
    public PresentDTO updatePresent(PresentDTO presentDTO) {
        Present present = presentRepository.findById(presentDTO.getId()).orElseThrow(ManagerException::new);
        Present presentRequest  = converter.convertToEntity(presentDTO);
        present.setValue((presentRequest.getValue()!=null)? presentRequest.getValue(): present.getValue());
        present.setStore((presentRequest.getStore()!=null)? presentRequest.getStore(): present.getStore());
        present.setPresentName((presentRequest.getPresentName()!=null)? presentRequest.getPresentName() : present.getPresentName());
        present.setPresentType((presentRequest.getPresentType()!=null)? presentRequest.getPresentType(): present.getPresentType());
        present = presentRepository.save(present);
        return converter.convertToDto(present);
    }
}

